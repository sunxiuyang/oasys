package cn.gson.oasys.model.dao.user;

import cn.gson.oasys.model.entity.user.Dept;
import cn.gson.oasys.model.entity.user.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoDaos extends JpaRepository<Position, Long>{
	 Page<Position> findByIsLock(Integer isLock, Pageable pa);
}
