package cn.gson.oasys.model.dao.user;

import cn.gson.oasys.model.entity.user.Dept;
import cn.gson.oasys.model.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeptDaos extends JpaRepository<Dept, Long>{
	 Page<Dept> findByIsLock(Integer isLock, Pageable pa);
}
