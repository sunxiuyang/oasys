<div class="box-body no-padding thistable">
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <tr>

                <th scope="col">名称</th>
                <th scope="col">层级</th>
                <th scope="col">描述</th>
                <th scope="col">操作</th>
            </tr>
		<#list positions as position>
            <tr>
                <td><span>${position.name}</span></td>
                <td><span>${(position.level)!''}</span></td>
                <td><span>${(position.describtion)!''}</span></td>
                <td><a  href="positionedit?positionid=${position.id}" class="label xiugai"><span
                        class="glyphicon glyphicon-edit"></span> 修改</a>
            </tr>
		</#list>
        </table>
    </div>
</div>
<#include "/common/paging.ftl"/>